# CREDENTIALS

variable "SUBSCRIPTION_ID" {
  type = string
  sensitive = true
}

variable "CLIENT_ID" {
  type = string
  sensitive = true
}

variable "CLIENT_SECRET" {
  type = string
  sensitive = true
}

variable "TENANT_ID" {
  type = string
  sensitive = true
}

# VIRTUAL MACHINE VARIABLES

variable "VM_SIZE" {
  type        = string
  description = "VM Size azure"
  default     = "Standard_DS1_v2"
}

variable "VM_UBUNTU_VERSION" {
  type    = string
  default = "18.04-LTS"
}

variable "VM_NAME" {
  type = string
  default = "my-first-vm"
}


# RESOURCE GROUP VARIABLES

variable "RS_NAME" {
  type = string
  default = "vm-getting-started"
}

variable "RS_LOCATION" {
  default       = "eastus"
  description   = "Location of the resource group."
}
