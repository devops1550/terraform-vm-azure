output "resource_group_name" {
  value = azurerm_resource_group.rg.name
}

output "public_ip_address" {
  value     = azurerm_linux_virtual_machine.myterraformvm.public_ip_address
  sensitive = true
}

output "tls_private_key" {
  value     = tls_private_key.example_ssh.private_key_pem
  sensitive = true
}

# WRITE FILES

resource "local_file" "tls_private_key" {
  content  = tls_private_key.example_ssh.private_key_pem
  filename = "private_key.pem"
}


resource "local_file" "public_ip_address" {
  content  = azurerm_linux_virtual_machine.myterraformvm.public_ip_address
  filename = "ip_addres.txt"
}
