Getting Started with AZ VM

### Azure CLI

```bash
docker run -it --rm -v ${PWD}:/work -w /work --entrypoint /bin/sh mcr.microsoft.com/azure-cli:latest
```

### Login to Azure

```bash
az login
az account list
```

### Output
```json
[
  {
    "cloudName": "AzureCloud",
    "id": "00000000-0000-0000-0000-000000000000",
    "isDefault": true,
    "name": "PAYG Subscription",
    "state": "Enabled",
    "tenantId": "00000000-0000-0000-0000-000000000000",
    "user": {
      "name": "user@example.com",
      "type": "user"
    }
  }
]
```

### Set suscription

```bash
SUBSCRIPTION_ID=
az account set --subscription="${SUBSCRIPTION_ID}"
```

We can now create the Service Principal which will have permissions to manage resources in the specified Subscription using the following command:

```
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/${SUBSCRIPTION_ID}"
```

### Output

```json
{
  "appId": "00000000-0000-0000-0000-000000000000",
  "displayName": "azure-cli-2017-06-05-10-41-15",
  "name": "http://azure-cli-2017-06-05-10-41-15",
  "password": "0000-0000-0000-0000-000000000000",
  "tenant": "00000000-0000-0000-0000-000000000000"
}

```

These values map to the Terraform variables like so:

* appId is the client_id defined above.
* password is the client_secret defined above.
* tenant is the tenant_id defined above.

#### Reference for get Azure Credentials

https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret